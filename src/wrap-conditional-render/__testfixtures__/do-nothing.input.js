// @flow

import React, { Component, type Element } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { SpotlightTarget } from "@atlaskit/onboarding";
import flow from "lodash/flow";
import isEqual from "lodash/isEqual";
import isNil from "lodash/isNil";
import noop from "lodash/noop";

import withAnalytics from "common/analytics/with-analytics";

import SlideDownOnMount from "../../../../../../common/components/animations/slide-down-on-mount";
import Button from "../../../../../../common/components/button";
import { glanceAnalyticsOnLoad } from "../../../../../common/ecosystem/ecosystem-analytics";
import { safeComponent } from "../../../../../common/util/safe-component";
import {
  expandShowMore,
  collapseShowMore
} from "../../../../../actions/context-items-action-creator";
import { glancesSelector } from "../../../../../ecosystem/ecosystem-extensions-selector";
import { isEcosystemRightPanelContextEnabled } from "../../../../../feature-flags";
import {
  visibleContextItemsSelector,
  conditionallyVisibleContextItemsSelector,
  isShowMoreExpandedSelector
} from "../../../../../issue-field/state/selectors/context-items-selector";
import type { ContextItem } from "../../../../../model/context-items-type";
import { isCompletedLoadingSelector } from "../../../../../selectors/issue-selector";
import { SHOW_MORE } from "../../../../constants";
import ContextItemsSkeleton from "../../../../skeleton/context-items-view";
import contextItemToElement from "../../context-item-to-element";
import Glance from "../../ecosystem/ecosystem-rich-glance-view";
import {
  ExpandButtonWrapper,
  VisibleWrapper,
  ConditionallyVisibleWrapper,
  WrapperWithKeyline
} from "../styled";

import type { Props } from "./types";

const shouldUpdateItemVisibility = (prevProps, nextProps): boolean => {
  const isCollapsed =
    !prevProps.isShowMoreExpanded && !nextProps.isShowMoreExpanded;
  const isCollapsing =
    prevProps.isShowMoreExpanded && !nextProps.isShowMoreExpanded;

  const didExtraItemBecomeVisibleWhileCollapsed =
    isCollapsed &&
    prevProps.visibleContextItems.length < nextProps.visibleContextItems.length;

  const didItemGetSwappedWhileCollapsed =
    isCollapsed &&
    prevProps.visibleContextItems.length ===
      nextProps.visibleContextItems.length &&
    !isEqual(prevProps.visibleContextItems, nextProps.visibleContextItems);

  const didChangeLoadingStage = prevProps.isComplete !== nextProps.isComplete;

  const shouldUpdateVisibleItems =
    isCollapsing ||
    didExtraItemBecomeVisibleWhileCollapsed ||
    didItemGetSwappedWhileCollapsed ||
    didChangeLoadingStage;

  return shouldUpdateVisibleItems;
};

type State = {
  visibleContextItems: ContextItem[],
  conditionallyVisibleContextItems: ContextItem[]
};

@safeComponent()
export class ContextItems extends Component<Props, State> {
  static displayName = "ContextItems";

  static defaultProps = {
    glances: [],
    onGlancesRendered: noop
  };

  // We don't want context items shifting above/below the show more link during the lifecycle
  // of the app, so store the values on initial load.
  state = {
    visibleContextItems: this.props.visibleContextItems,
    conditionallyVisibleContextItems: this.props
      .conditionallyVisibleContextItems
  };

  componentDidMount() {
    const { glances, onGlancesRendered } = this.props;

    if (glances.length > 0) {
      onGlancesRendered(glanceAnalyticsOnLoad(glances));
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    if (shouldUpdateItemVisibility(this.props, nextProps)) {
      this.setState({
        visibleContextItems: nextProps.visibleContextItems,
        conditionallyVisibleContextItems:
          nextProps.conditionallyVisibleContextItems
      });
    }
  }

  getElementsFromContextItems = (contextItems: ContextItem[]): Element<any> =>
    // $FlowFixMe BENTO-2075
    contextItems.map(contextItemToElement).filter(element => !isNil(element));

  renderVisibleItems() {
    const visibleElements = this.getElementsFromContextItems(
      this.state.visibleContextItems
    );

    return (
      <VisibleWrapper>
        {visibleElements}
        {this.renderGlances()}
      </VisibleWrapper>
    );
  }

  renderConditionallyVisibleItems() {
    if (!this.props.isShowMoreExpanded) {
      return null;
    }

    const conditionallyVisibleElements = this.getElementsFromContextItems(
      this.state.conditionallyVisibleContextItems
    );

    return (
      <SlideDownOnMount>
        <ConditionallyVisibleWrapper>
          {conditionallyVisibleElements}
        </ConditionallyVisibleWrapper>
      </SlideDownOnMount>
    );
  }

  renderExpander() {
    const conditionallyVisibleElements = this.getElementsFromContextItems(
      this.state.conditionallyVisibleContextItems
    );

    // $FlowFixMe BENTO-2075
    if (conditionallyVisibleElements.length === 0) {
      // BENTO-1786: return an empty div as an anchor for the spotlight when show more button doesn't exist.
      return <div />;
    }

    const showMoreButton = (
      <Button
        externalId="showmorelink.expand"
        appearance="link"
        onClick={this.props.onExpand}
      >
        <FormattedMessage
          id="issue.text-on-show-more-details-button"
          defaultMessage="Show more"
        />
      </Button>
    );

    const hideMoreButton = (
      <Button
        externalId="showmorelink.collapse"
        appearance="link"
        onClick={this.props.onCollapse}
      >
        <FormattedMessage
          id="issue.text-on-show-fewer-details-button"
          defaultMessage="Show less"
        />
      </Button>
    );

    return (
      <WrapperWithKeyline>
        <ExpandButtonWrapper>
          {this.props.isShowMoreExpanded ? hideMoreButton : showMoreButton}
        </ExpandButtonWrapper>
      </WrapperWithKeyline>
    );
  }

  renderGlances() {
    if (!isEcosystemRightPanelContextEnabled()) {
      return null;
    }
    return this.props.glances.map(glance => (
      <Glance
        key={glance.name}
        name={glance.name}
        appKey={glance.appKey}
        moduleKey={glance.moduleKey}
        icon={glance.icon}
        label={glance.label}
        status={glance.status}
        externalId="issue.ecosystem.glance.field"
      />
    ));
  }

  renderSkeleton = () => <ContextItemsSkeleton />;

  render() {
    return (
      <div>
        {this.renderVisibleItems()}
        {this.renderConditionallyVisibleItems()}
        {this.props.isComplete ? (
          <SpotlightTarget name={SHOW_MORE}>
            {this.renderExpander()}
          </SpotlightTarget>
        ) : (
          this.renderSkeleton()
        )}
      </div>
    );
  }
}

export default flow(
  withAnalytics(trigger => ({
    onGlancesRendered: glancesData =>
      trigger("ecosystem.glances", { ...glancesData })
  })),
  connect(
    state => ({
      glances: glancesSelector(state),
      visibleContextItems: visibleContextItemsSelector(state),
      conditionallyVisibleContextItems: conditionallyVisibleContextItemsSelector(
        state
      ),
      isShowMoreExpanded: isShowMoreExpandedSelector(state),
      isComplete: isCompletedLoadingSelector(state)
    }),
    dispatch => ({
      onExpand: () => dispatch(expandShowMore()),
      onCollapse: () => dispatch(collapseShowMore())
    })
  ),
  safeComponent()
)(ContextItems);
