const prettier = require("prettier");

module.exports = source => {
  const config = prettier.resolveConfig.sync(process.cwd()) || {};
  return prettier.format(source, { parser: "babel", ...config });
};
