import JiraMajorIcon from "@atlaskit/icon/glyph/jira/minor";
import JiraMinorIcon from "@atlaskit/icon/glyph/jira/major";

export default () => (
  <Fragment>
    <JiraMajorIcon />
    <JiraMinorIcon />
  </Fragment>
);
