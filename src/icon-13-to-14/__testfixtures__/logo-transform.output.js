import somethingElse from "./somewhere";
import { somethingMoreElse } from "./somewhereElse";
import AtlassianIcon from "@atlaskit/logo/dist/esm/AtlassianLogo/Icon";

export default (
  <div>
    <AtlassianIcon size="small" />
    <AtlassianIcon size="xsmall" secondProp="a string" />
    <AtlassianIcon size="small" />
    <AtlassianIcon size="medium" />
    <AtlassianIcon size="xlarge" />
  </div>
);
