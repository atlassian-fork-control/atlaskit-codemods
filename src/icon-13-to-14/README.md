# Upgrade from @atlaskit/icon 13 to 14

This codemod makes an educated guess at the changes you need to make to upgrade from `@atlaskit/icon` v13 to v14. It performs 3 transforms:

## Usage


## Running

In the directory of repo you want to run the codemod on:

```
jscodeshift -t ../atlaskit-codemods/src/icon-13-to-14/codemod.js src --ignore-config .eslintignore
```

## Examples

These are examples of what types of expressions are transformed by this codemod.

Use of file type icons

```diff
- import SmallIcon from "@atlaskit/icon/glyph/file-types/16/file-16-source-code";
- import LargerIcon from "@atlaskit/icon/glyph/file-types/24/file-24-gif";
+ import SmallIcon from "@atlaskit/icon-file-type/glyph/source-code/16";
+ import LargerIcon from "@atlaskit/icon-file-type/glyph/gif/24";

export default () => (
  <div>
    <SmallIcon />
    <LargerIcon />
  </div>
);
```

Use of logo icons

```diff
- import AtlassianIcon from "@atlaskit/icon/glyph/atlassian";
+ import AtlassianIcon from "@atlaskit/logo/dist/esm/AtlassianLogo/Icon";

export default (
  <div>
-    <AtlassianIcon />
-    <AtlassianIcon size="small" secondProp="a string" />
-    <AtlassianIcon size="medium" />
-    <AtlassianIcon size="large" />
-    <AtlassianIcon size="xlarge" />
+    <AtlassianIcon size="small" />
+    <AtlassianIcon size="xsmall" secondProp="a string" />
+    <AtlassianIcon size="small" />
+    <AtlassianIcon size="medium" />
+    <AtlassianIcon size="xlarge" />
  </div>
);
```

Use of jira priority icon are swapped

```diff
- import JiraMajorIcon from "@atlaskit/icon/glyph/jira/major";
- import JiraMinorIcon from "@atlaskit/icon/glyph/jira/minor";
+ import JiraMajorIcon from "@atlaskit/icon/glyph/jira/minor";
+ import JiraMinorIcon from "@atlaskit/icon/glyph/jira/major";

export default () => (
  <Fragment>
    <JiraMajorIcon />
    <JiraMinorIcon />
  </Fragment>
);
```
