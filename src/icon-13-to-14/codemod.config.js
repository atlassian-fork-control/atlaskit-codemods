const options = {
  // components to transform
  components,
  // parser for jscodeshift to use
  parser: "flow",
  // run prettier on output, will respect local prettier config
  prettier: true,
  // named of import to add
  transitionComponentName
};
