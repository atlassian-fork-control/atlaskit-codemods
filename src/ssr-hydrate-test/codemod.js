const prettier = require("../prettier/prettier");

const transform = (j, root, path) => {
  const packageNameRegex = /packages\/core\/([^\/]*)\//;
  const matches = path.match(packageNameRegex);
  const packageName = matches ? matches[1] : "COMPONENT";
  const code = function(codeString) {
    return j(codeString)
      .find(j.Program)
      .get().node.body;
  };

  const source = code(`
// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { getExamplesFor } from '@atlaskit/build-utils/getExamples';
import { ssr } from '@atlaskit/ssr';

jest.spyOn(global.console, 'error');

afterEach(() => {
  jest.resetAllMocks();
});

test('should ssr then hydrate ${packageName} correctly', async () => {
  const [example] = await getExamplesFor('${packageName}');
  // $StringLitteral
  const Example = require(example.filePath).default; // eslint-disable-line import/no-dynamic-require

  const elem = document.createElement('div');
  elem.innerHTML = await ssr(example.filePath);

  ReactDOM.hydrate(<Example />, elem);
  expect(console.error).not.toBeCalled(); // eslint-disable-line no-console
});
`);
  root.find(j.Program).get().node.body = source;
  return true;
};

module.exports = (file, api, options) => {
  const j = api.jscodeshift;
  const root = j(file.source);
  const mutations = transform(j, root, file.path);
  return mutations ? prettier(root.toSource()) : null;
};

module.exports.parser = "flow";
